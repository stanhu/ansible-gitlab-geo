## Introduction

This repository includes a few Ansible playbooks that make it easier to spin
up multiple instances running GitLab Geo.

The goal is to spin up two clusters of GitLab Geo: one for the primary and
one for the secondary.

### Primary Cluster

The primary cluster is comprised of 5 different nodes:

* 2 x GitLab worker machines
* 1 x NFS/Gitaly server
* 1 x Redis node
* 1 x PostgreSQL node

### Secondary Cluster

The secondary has the same node configuration as the primary, but
it has an additional node for the Geo PostgreSQL tracking database.

### Scripts

#### bootstrap.yml

This Ansible playbook does the following:

1. Creates all the nodes in Google Cloud. One cluster is in `us-west1-a` and the other is in `us-east1-d`.
2. Creates a Google Load Balancer to direct traffic to the GitLab worker machine.
3. Registers the Google Load Balancer in DNS.
4. Installs GitLab EE on all the servers

### nfs-server.yml

This Ansible playbook installs an NFS server and shares out all the GitLab mounts.
See the `templates/nfs-server/exports` file for the directories it exports.

Example run:

```sh
ansible-playbook -i inventory/primary_inventory nfs-server.yml
```

### pg.yml

This Ansible playbook installs PostgreSQL using the GitLab Omnibus package.

### redis.yml

This Ansible playbook installs Redis using the GitLab Omnibus package.

### gitlab-worker.yml

This Ansible playbook does the following:

1. Installs an NFS client
2. Installs shared GitLab and SSH secrets

It expects the following files to exist:

1. files/secrets/gitlab/gitlab-secrets.json
2. files/secrets/ssh/ssh_host_{dsa,ecdsa,ed25519,rsa}_key and their public keys

# TODO

1. GitLab workers need to have HTTP/HTTPS opened in firewall settings
1. GCP health checks talk to port 8060, but GitLab /nginx_status settings may not be configured right

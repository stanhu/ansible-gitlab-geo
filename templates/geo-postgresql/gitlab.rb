geo_secondary_role['enable'] = true
geo_postgresql['enable'] = true

gitaly['enable'] = false
gitlab_rails['enable'] = false
unicorn['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
mailroom['enable'] = false
nginx['enable'] = false
pages_nginx['enable'] = false
postgresql['enable'] = false
gitlab_pages['enable'] = false
registry['enable'] = false
redis['enable'] = false
gitaly['enable'] = false

gitlab_rails['auto_migrate'] = false

geo_postgresql['listen_address'] = '*'
geo_postgresql['trust_auth_cidr_addresses'] = {{ trust_auth_cidr_addresses | to_json }}

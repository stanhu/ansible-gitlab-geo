external_url "{{ gitlab_external_url }}"

# Prevent GitLab from starting if NFS data mounts are not available
high_availability['mountpoint'] = '/var/opt/gitlab/git-data'

# Disable components that will not be on the GitLab application server
roles ['application_role']

nginx['enable'] = true
unicorn['enable'] = true

# PostgreSQL connection details
gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'unicode'
gitlab_rails['db_host'] = "{{ postgresql_host }}"
gitlab_rails['db_password'] = ''

# Redis connection details
gitlab_rails['redis_port'] = '6379'
gitlab_rails['redis_host'] = "{{ redis_host }}"
gitlab_rails['redis_password'] = nil

{% if geo_secondary is defined %}
gitlab_rails['db_password'] = '{{ db_user_password }}'
# geo_secondary_role['enable'] = true
geo_secondary['db_host'] = '{{ geo_postgresql_host }}'
geo_secondary['db_port'] = 5431
geo_secondary['db_fdw'] = true
geo_postgresql['enable'] = false

{% if geo_logcursor is defined %}
geo_logcursor['enable'] = true
{% else %}
geo_logcursor['enable'] = false
{% endif %}

{% endif %}

gitaly['enable'] = false

git_data_dirs({
  'default' => { 'path' => '/var/opt/gitlab/git-data', 'gitaly_address' => 'tcp://{{ nfs_server }}:9999' }
})

nginx['status'] = {
  "enable" => true,
  "listen_addresses" => ["0.0.0.0"],
  "fqdn" => "{{ nginx_status_fqdn }}",
  "port" => 8060,
  "options" => {
    "stub_status" => "on", # Turn on stats
    "server_tokens" => "off", # Don't show the version of NGINX
    "access_log" => "off", # Disable logs for stats
    "allow" => "0.0.0.0",
    "deny" => "all" # Deny access to anyone else
  }
}

## Object storage
gitlab_rails['lfs_object_store_enabled'] = true
gitlab_rails['lfs_object_store_remote_directory'] = 'stanhu-geo-ha-lfs'
gitlab_rails['lfs_object_store_connection'] = {
   'provider' => 'Google',
   'google_storage_access_key_id' => '{{ google_storage_access_key_id }}',
   'google_storage_secret_access_key' => '{{ google_storage_secret_access_key }}'
}

gitlab_rails['lfs_object_store_background_upload'] = true

gitlab_rails['artifacts_enabled'] = true
gitlab_rails['artifacts_object_store_enabled'] = true
gitlab_rails['artifacts_object_store_remote_directory'] = 'stanhu-geo-ha-artifacts'
gitlab_rails['artifacts_object_store_connection'] = {
  'provider' => 'Google',
  'google_storage_access_key_id' => '{{ google_storage_access_key_id }}',
  'google_storage_secret_access_key' => '{{ google_storage_secret_access_key }}'
}

gitlab_rails['artifacts_object_store_background_upload'] = true

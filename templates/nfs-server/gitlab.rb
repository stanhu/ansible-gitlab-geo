external_url "{{ gitlab_external_url }}"

gitaly['enable'] = true
gitlab_rails['enable'] = true
unicorn['enable'] = true

gitlab_rails['db_host'] = "{{ postgresql_host }}"

# Redis connection details
gitlab_rails['redis_port'] = '6379'
gitlab_rails['redis_host'] = "{{ redis_host }}"
gitlab_rails['redis_password'] = nil

sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
mailroom['enable'] = false
nginx['enable'] = false
pages_nginx['enable'] = false
postgresql['enable'] = false
gitlab_pages['enable'] = false
registry['enable'] = false
redis['enable'] = false
gitaly['enable'] = true

gitlab_rails['auto_migrate'] = false
gitaly['listen_addr'] = '0.0.0.0:9999'
